package com.alonjacovi.jumpergdx.View;

import com.alonjacovi.jumpergdx.Model.LevelEntity;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public interface EntityAnimator
{
	
	public void render(TextureAtlas atlas, int tileSize, SpriteBatch batch,
			LevelEntity entity);

	public TextureRegion getTexture(TextureAtlas atlas, LevelEntity entity);


}
