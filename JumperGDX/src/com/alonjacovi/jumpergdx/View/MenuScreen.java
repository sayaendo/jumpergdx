package com.alonjacovi.jumpergdx.View;

import com.alonjacovi.jumpergdx.JumperGdxGame;
import com.alonjacovi.jumpergdx.Controller.MenuInputProcessor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class MenuScreen implements Screen
{
	JumperGdxGame game;
	OrthographicCamera cam;
	SpriteBatch batch;
	Sprite sprite;
	Music music;
	
	public MenuScreen(JumperGdxGame game){
		this.game = game;
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		batch.begin();
		sprite.draw(batch);
		batch.end();
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void show() {
//		music = Gdx.audio.newMusic(Gdx.files.internal("data/placeholder.ogg"));
//		music.setLooping(true);
//		music.play();
		
		//Camera set to (0,0) being bottom left
		float h = Gdx.graphics.getHeight();
		float w = Gdx.graphics.getWidth();
		cam = new OrthographicCamera(w, h);
		cam.position.set(w/2, h/2, 0);
		
		cam.update();
		
		Gdx.input.setInputProcessor(new MenuInputProcessor(game));
		
		sprite = new Sprite(new Texture(Gdx.files.internal("data/yooo.png")));
		sprite.setBounds(0, 0, w, h);
		
		batch = new SpriteBatch();
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
		sprite.getTexture().dispose();
		music.dispose();
	}

}
