package com.alonjacovi.jumpergdx.View;

import com.alonjacovi.jumpergdx.Controller.PlayerController.PlayerState;
import com.alonjacovi.jumpergdx.Model.LevelEntity;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class PlayerAnimator implements EntityAnimator
{
	
	private Animation walkingRight, walkingLeft;
	private TextureRegion idleLeft, idleRight, jumpLeft, jumpRight;
	
	boolean isCached = false;

	@Override
	public void render(TextureAtlas atlas, int tileSize, SpriteBatch batch,
			LevelEntity entity) {
		batch.draw(getTexture(atlas, entity), 
				(entity.getPosition().x*tileSize), (entity.getPosition().y*tileSize));
	}

	@Override
	public TextureRegion getTexture(TextureAtlas atlas, LevelEntity entity) {
		
		if (!isCached)
		{
			idleRight = atlas.findRegion("mariIdle");
			idleLeft = atlas.findRegion("mariIdle2");
			jumpRight = atlas.findRegion("mariJump");
			jumpLeft = atlas.findRegion("mariJump2");
			
			walkingRight = new Animation(0.2f, new TextureRegion[] {atlas.findRegion("mariRun"), idleRight});
			walkingLeft = new Animation(0.2f, new TextureRegion[] {atlas.findRegion("mariRun2"), idleLeft});
			
			walkingRight.setPlayMode(PlayMode.LOOP);
			walkingLeft.setPlayMode(PlayMode.LOOP);
			
			isCached = true;
		}
		
		float timer = entity.getController().getTimer();
		
		switch ((PlayerState) entity.getController().getState())
		{
		case LEFT:
				return walkingLeft.getKeyFrame(timer);
		case RIGHT:
				return walkingRight.getKeyFrame(timer);
		case IDLE:
			if (entity.getDirection().x <= 0)
				return idleLeft;
			else return idleRight;
		case JUMP:
			if (entity.getDirection().x <= 0)
				return jumpLeft;
			else return jumpRight;
		case DEAD: return idleLeft;
		}
		
		throw new IllegalStateException("unknown entity state: "+entity.getController().getState());
	}

}
