package com.alonjacovi.jumpergdx.View;

import com.alonjacovi.jumpergdx.Model.Level;
import com.badlogic.gdx.Screen;

public class WorldScreen implements Screen
{
	Level level;
	
	public WorldScreen(Level level) {
		this.level = level;
	}

	@Override
	public void render(float delta) {
		
		level.update(delta);
		
	}

	@Override
	public void resize(int width, int height) {
		//no android support
	}

	@Override
	public void show() {
		//no android support
	}

	@Override
	public void hide() {
		//no android support
	}

	@Override
	public void pause() {
		//no android support
	}

	@Override
	public void resume() {
		//no android support
	}

	@Override
	public void dispose() {
		level.dispose();
	}

}
