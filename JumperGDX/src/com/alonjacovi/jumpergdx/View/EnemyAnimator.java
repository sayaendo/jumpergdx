package com.alonjacovi.jumpergdx.View;

import com.alonjacovi.jumpergdx.Controller.EnemyController.EnemyState;
import com.alonjacovi.jumpergdx.Model.LevelEntity;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class EnemyAnimator implements EntityAnimator
{
	private TextureRegion left, right;
	
	private boolean isCached = false;

	@Override
	public void render(TextureAtlas atlas, int tileSize, SpriteBatch batch,
			LevelEntity entity) {
		
		batch.draw(getTexture(atlas, entity), entity.getPosition().x*tileSize, entity.getPosition().y*tileSize);
	}

	@Override
	public TextureRegion getTexture(TextureAtlas atlas, LevelEntity entity) {
		
		if (!isCached)
		{
			left = atlas.findRegion("eny");
			right = atlas.findRegion("eny2");
			isCached = true;
		}
		
		switch ((EnemyState) entity.getController().getState())
		{
		case LEFT:
			return left;
		case RIGHT:
			return right;
		default:
			return left;
		}
	}

}
