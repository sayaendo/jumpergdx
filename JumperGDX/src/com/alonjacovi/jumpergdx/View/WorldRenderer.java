package com.alonjacovi.jumpergdx.View;

import com.alonjacovi.jumpergdx.Model.Enemy;
import com.alonjacovi.jumpergdx.Model.Level;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;

public class WorldRenderer
{
	Level level;
	
	OrthographicCamera cam;
	OrthogonalTiledMapRenderer renderer;
	TextureAtlas atlas;
	SpriteBatch batch;
	
	public WorldRenderer(Level level)
	{
		this.level = level;
		
		//Camera set to (0,0) being bottom left
		float h = Gdx.graphics.getHeight();
		float w = Gdx.graphics.getWidth();
		cam = new OrthographicCamera(w, h);
		cam.position.set(w/2, h/2, 0);
		//following 2 lines are for zoom 2x, remove to revert
		cam.viewportHeight = h/2;
		cam.viewportWidth = w/2;
		
		cam.update();
		
		renderer = new OrthogonalTiledMapRenderer(level.getMap());
		
		atlas = new TextureAtlas(Gdx.files.internal("data/entities.pack"));
		
		batch = new SpriteBatch();
	}
	
	public void render()
	{
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		float playerPosX = (level.getPlayer().getPosition().x*level.getTileSize());
		float playerPosY = (level.getPlayer().getPosition().y*level.getTileSize());
		
		cam.position.set(new Vector2(playerPosX, playerPosY), 0);
		//cam.position.lerp(new Vector3(playerPosX, playerPosY, 0), 0.3f);
		cam.update();
		
		renderer.setView(cam);
		renderer.render();
		
		batch.setProjectionMatrix(cam.combined);
		
		batch.begin();
		
		for (Enemy enemy : level.getEnemies())
		{
			enemy.getEntity().render(atlas, level.getTileSize(), batch);
		}
		
		level.getPlayer().render(atlas, level.getTileSize(), batch);
		
		batch.end();
	}
	
	public void dispose()
	{
		
	}

}
