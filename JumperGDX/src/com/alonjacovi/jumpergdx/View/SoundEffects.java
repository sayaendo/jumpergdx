package com.alonjacovi.jumpergdx.View;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;

public enum SoundEffects

{
	HIT_ENEMY("world/placeholder.wav");
	
	//** no dispose method - SFX are used everywhere in the entire game
	//to add specific SFX that are only used in certain positions, create another enum
	
	private String path;
	private Sound sound;
	
	private SoundEffects(String path)
	{
		this.path = path;
		
		sound = Gdx.audio.newSound(Gdx.files.internal(path));
	}
	
	public String getPath()
	{
		return path;
	}
	
	public void play() {
		sound.play();
	}

}
