package com.alonjacovi.jumpergdx;

import com.alonjacovi.jumpergdx.Model.Level;
import com.alonjacovi.jumpergdx.Model.LevelEntity;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Rectangle;

public enum Collision
{
	INSTANCE;
	
	public static boolean isCollidingLeft(Level level, LevelEntity entity)
	{
		Rectangle bounds = entity.getBounds();

		float approx = 1f / (float) level.getTileSize();
		bounds.x += approx;
		bounds.y += approx * 4 ;
		bounds.width -= approx * 2;
		bounds.height -= approx * 8;
		
		TiledMapTileLayer collisionLayer = (TiledMapTileLayer) level.getMap().getLayers().get("Collision");
		
		float step = bounds.height / (bounds.height + approx* 8);
		
		for (float i = bounds.y; i <= (bounds.y + bounds.height); i = i + step)
		{
			if (collisionLayer.getCell((int) (bounds.x), (int) (i)) != null)
				return true;
		}
		
		return false;
	}
	
	public static boolean isCollidingRight(Level level, LevelEntity entity)
	{
		float approx = 1f / (float) level.getTileSize();

		Rectangle bounds = entity.getBounds();
		bounds.x += approx;
		bounds.y += approx * 4 ;
		bounds.width -= approx * 2;
		bounds.height -= approx * 8;
		
		TiledMapTileLayer collisionLayer = (TiledMapTileLayer) level.getMap().getLayers().get("Collision");
		
		float step = bounds.height / (bounds.height + approx* 8);
		
		//iterates for as many times as there are tiles in the original bounds
		for (float i = bounds.y; i <= (bounds.y + bounds.height); i = i + step)
		{
			if (collisionLayer.getCell((int) (bounds.x + bounds.width), (int) (i)) != null)
				return true;
		}
		
		return false;
	}
	
	public static boolean isCollidingUp(Level level, LevelEntity entity)
	{
		float approx = 0.5f / (float) level.getTileSize();

		Rectangle bounds = entity.getBounds();		
		bounds.x += approx * 8;
		bounds.y += approx;
		bounds.width -= approx * 16;
		bounds.height -= approx*2;
		
		TiledMapTileLayer collisionLayer = (TiledMapTileLayer) level.getMap().getLayers().get("Collision");
		
		float step = bounds.width / (bounds.width + approx*16);
		
		//iterates for as many times as there are tiles in the original bounds
		for (float i = bounds.x; i <= (bounds.x + bounds.width); i = i + step)
		{
			
			if (collisionLayer.getCell((int) (i), (int) (bounds.y + bounds.height)) != null)
				return true;
		}
		
		return false;
	}
	
	public static boolean isCollidingDown(Level level, LevelEntity entity)
	{
		float approx = 0.5f / (float) level.getTileSize();

		Rectangle bounds = entity.getBounds();
		bounds.x += approx * 8;
		bounds.y += approx;
		bounds.width -= approx * 16;
		bounds.height -= approx*2;
		
		
		TiledMapTileLayer collisionLayer = (TiledMapTileLayer) level.getMap().getLayers().get("Collision");
		
		float step = bounds.width / (bounds.width + approx*16);
		
		//iterates for as many times as there are tiles in the original bounds
		for (float i = bounds.x; i <= (bounds.x + bounds.width); i = i + step)
		{
			if (collisionLayer.getCell((int) (i), (int) (bounds.y)) != null)
				return true;
		}
		
		return false;
	}

}
