package com.alonjacovi.jumpergdx;

import com.alonjacovi.jumpergdx.View.MenuScreen;
import com.badlogic.gdx.Game;

public class JumperGdxGame extends Game {

	
	@Override
	public void create() {		
		
		setScreen(new MenuScreen(this));
		
//		setScreen(new WorldScreen(new Level()));
		
	}
	
}
