package com.alonjacovi.jumpergdx.Controller;

import java.util.LinkedList;

public abstract class AbstractEntityController implements EntityController
{

	private LinkedList<ActiveState> history = new LinkedList<ActiveState>();
	
	@Override
	public ActiveState getState() {
		return history.peekLast();
	}

	@Override
	public ActiveState getPreviousState() {
		return history.peekFirst();
	}

	@Override
	public void setState(ActiveState state) {
		if (history.size() > 2)
			history.pop();
		history.add(state);

		resetTimer();
	}

	private float timer;

	@Override
	public float getTimer() {
		return timer;
	}

	@Override
	public void resetTimer() {
		timer = 0;
	}
	
	@Override
	public void addTimer(float delta) {
		timer += delta;
	}

}
