package com.alonjacovi.jumpergdx.Controller;

import com.alonjacovi.jumpergdx.JumperGdxGame;
import com.alonjacovi.jumpergdx.Model.Level;
import com.alonjacovi.jumpergdx.View.WorldScreen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;

public class MenuInputProcessor implements InputProcessor
{
	JumperGdxGame game;
	
	public MenuInputProcessor(JumperGdxGame game){
		this.game = game;
	}

	@Override
	public boolean keyDown(int keycode) {
		
		if (keycode == Keys.ENTER)
		{
			game.setScreen(new WorldScreen(new Level(game)));
		}
		
		return true;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

}
