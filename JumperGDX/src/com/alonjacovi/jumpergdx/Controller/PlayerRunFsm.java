package com.alonjacovi.jumpergdx.Controller;

import com.alonjacovi.jumpergdx.Model.Level;
import com.alonjacovi.jumpergdx.Model.LevelEntity;
import com.alonjacovi.jumpergdx.Model.Species;
import com.badlogic.gdx.Input.Keys;

public class PlayerRunFsm extends AbstractEntityController
{
	
	public enum RunState implements ActiveState {
		SLOW {

			@Override
			public void enterState(LevelEntity entity) {
				entity.getPassives().get(0).setState(this);
			}

			@Override
			public void exitState(LevelEntity entity) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void update(LevelEntity entity, float delta) {
				// TODO Auto-generated method stub
				
			}
			
		},
		FAST {

			@Override
			public void enterState(LevelEntity entity) {
				entity.getPassives().get(0).setState(this);
			}

			@Override
			public void exitState(LevelEntity entity) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void update(LevelEntity entity, float delta) {
				// TODO Auto-generated method stub
				
			}
			
		};
	}
	
	private LevelEntity entity;
	
	public PlayerRunFsm(LevelEntity entity)
	{
		this.entity = entity;
		setState(RunState.SLOW);
	}

	@Override
	public void handleInputPressed(int keycode) {
		
		if (keycode == Keys.S)
			RunState.FAST.enterState(entity);

	}

	@Override
	public void handleInputReleased(int keycode) {
		
		if (keycode == Keys.S)
			RunState.SLOW.enterState(entity);
		
	}

	@Override
	public void update(float delta, Level level) {
		//addTimer(delta);
		
	}

	@Override
	public void gotHit(Species entity) {
		
	}

}
