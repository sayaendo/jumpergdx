package com.alonjacovi.jumpergdx.Controller;

import com.alonjacovi.jumpergdx.Model.LevelEntity;

public interface ActiveState
{
	
	public void enterState(LevelEntity entity);
	public void exitState(LevelEntity entity);
	public void update(LevelEntity entity, float delta);

}
