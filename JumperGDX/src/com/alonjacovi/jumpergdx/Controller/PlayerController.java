package com.alonjacovi.jumpergdx.Controller;

import com.alonjacovi.jumpergdx.Model.Level;
import com.alonjacovi.jumpergdx.Model.LevelEntity;
import com.alonjacovi.jumpergdx.Model.Species;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;

public class PlayerController extends AbstractEntityController
{
	
	public enum PlayerState implements ActiveState {
		LEFT 
		{
			@Override
			public void enterState(LevelEntity entity) {
				entity.getController().setState(this);
				
				entity.getVelocity().x = -5f;
			}

			@Override
			public void exitState(LevelEntity entity) {
				entity.getVelocity().x = 0;
			}

			@Override
			public void update(LevelEntity entity, float delta) {
				if (entity.isCollidingLeft())
					IDLE.enterState(entity);
				
				if (!entity.isCollidingDown())
					entity.getController().setState(JUMP);
			}
			
		},
		RIGHT 
		{
			@Override
			public void enterState(LevelEntity entity) {
				entity.getController().setState(this);
				
				entity.getVelocity().x = 5f;
			}

			@Override
			public void exitState(LevelEntity entity) {
				entity.getVelocity().x = 0;
				
			}

			@Override
			public void update(LevelEntity entity, float delta) {
				if (entity.isCollidingRight())
					IDLE.enterState(entity);
				
				if (!entity.isCollidingDown())
					entity.getController().setState(JUMP);
			}
			
		},
		JUMP 
		{
			@Override
			public void enterState(LevelEntity entity) {
				entity.getController().setState(this);
				
				entity.getAcceleration().x = 0;
				
				entity.getAcceleration().y = 23f;
				entity.getVelocity().y = 7;
			}

			@Override
			public void exitState(LevelEntity entity) {
				
			}

			@Override
			public void update(LevelEntity entity, float delta) {
				
				if (Gdx.input.isKeyPressed(Keys.LEFT))
					entity.getVelocity().x = -5f;
				else {
					if (Gdx.input.isKeyPressed(Keys.RIGHT))
						entity.getVelocity().x = 5f;
					else entity.getVelocity().x = 0;
				}
				
				if ((entity.isCollidingDown())&&(entity.getAcceleration().y == 0))
				{
					if (Gdx.input.isKeyPressed(Keys.LEFT))
						LEFT.enterState(entity);
					else if (Gdx.input.isKeyPressed(Keys.RIGHT))
						RIGHT.enterState(entity);
					else IDLE.enterState(entity);
				}
				
				if ((!Gdx.input.isKeyPressed(Keys.A))||(entity.getController().getTimer() > 0.2f))
				{
					entity.getAcceleration().y = 0;
					entity.getVelocity().y -= 0.5f;
				}
			}
		},
		IDLE {

			@Override
			public void enterState(LevelEntity entity) {
				entity.getController().setState(this);
				
				entity.getVelocity().set(0, 0);
				entity.getAcceleration().set(0, 0);
			}

			@Override
			public void exitState(LevelEntity entity) {
			}

			@Override
			public void update(LevelEntity entity, float delta) {
			}
			
		},
		
		DEAD {

			@Override
			public void enterState(LevelEntity entity) {
				entity.getController().setState(this);
			}

			@Override
			public void exitState(LevelEntity entity) {
				
			}

			@Override
			public void update(LevelEntity entity, float delta) {
				
			}
			
		};
		
	}
	
	private LevelEntity entity;
	
	public PlayerController(LevelEntity entity) {
		this.entity = entity;
		
		setState(PlayerState.IDLE);
	}

	@Override
	public void handleInputPressed(int keycode) {
		switch (keycode)
		{
		case Keys.LEFT:
			if ((getState() == PlayerState.IDLE)||
			    (getState() == PlayerState.LEFT)||
			    (getState() == PlayerState.RIGHT))
				
				PlayerState.LEFT.enterState(entity);
			
			break;
		case Keys.RIGHT:
			
			if ((getState() == PlayerState.IDLE)||
				    (getState() == PlayerState.LEFT)||
				    (getState() == PlayerState.RIGHT))
					
					PlayerState.RIGHT.enterState(entity);
			
			break;
		case Keys.A:
			
			if ((getState() == PlayerState.IDLE)||
				(getState() == PlayerState.LEFT)||
				(getState() == PlayerState.RIGHT))
				
				PlayerState.JUMP.enterState(entity);
		}
	}

	@Override
	public void handleInputReleased(int keycode) {
		switch (keycode)
		{
		case Keys.LEFT:
			
			if (getState() == PlayerState.LEFT)
				
				PlayerState.LEFT.exitState(entity);
			
			break;
		case Keys.RIGHT:
			
			if (getState() == PlayerState.RIGHT)
				
				PlayerState.RIGHT.exitState(entity);
				
			break;
		}
	}

	@Override
	public void update(float delta, Level level) {
		addTimer(delta);
		
		getState().update(entity, delta);
		
		if ((entity.getVelocity().isZero())&&(entity.getAcceleration().isZero())&&(getState() != PlayerState.DEAD))
			PlayerState.IDLE.enterState(entity);
	}

	@Override
	public void gotHit(Species hittingEntity) {
		
		if (entity.getBounds().getY() > hittingEntity.getBounds().getY()+(hittingEntity.getBounds().getHeight()*0.8f))
		{
			PlayerState.JUMP.enterState(entity);
//			SoundEffects.HIT_ENEMY.play();
		}
		else PlayerState.DEAD.enterState(entity);
		
	}

}