package com.alonjacovi.jumpergdx.Controller;

import com.alonjacovi.jumpergdx.Model.Level;
import com.alonjacovi.jumpergdx.Model.LevelEntity;
import com.alonjacovi.jumpergdx.Model.Species;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;

public class EnemyController extends AbstractEntityController
{
	
	public enum EnemyState implements ActiveState
	{
		SPAWN
		{
			@Override
			public void enterState(LevelEntity entity) {
			}

			@Override
			public void exitState(LevelEntity entity) {
			}

			@Override
			public void update(LevelEntity entity, float delta) {
			}
			
		},
		LEFT
		{

			@Override
			public void enterState(LevelEntity entity) {
				entity.getController().setState(this);
				
				entity.getVelocity().x = -3f;
			}

			@Override
			public void exitState(LevelEntity entity) {
			}

			@Override
			public void update(LevelEntity entity, float delta) {

			}
			
		},
		RIGHT
		{
			@Override
			public void enterState(LevelEntity entity) {
				entity.getController().setState(this);
				
				entity.getVelocity().x = 3f;
			}

			@Override
			public void exitState(LevelEntity entity) {

			}

			@Override
			public void update(LevelEntity entity, float delta) {

			}
			
		},
		DYING
		{

			@Override
			public void enterState(LevelEntity entity) {
				entity.getController().setState(this);		
				
				entity.getVelocity().x = 0;
			}

			@Override
			public void exitState(LevelEntity entity) {
				
			}

			@Override
			public void update(LevelEntity entity, float delta) {
				
				
				if (entity.getController().getTimer() >= 0.5f)
					DEAD.enterState(entity);
				
			}
			
		},
		DEAD
		{

			@Override
			public void enterState(LevelEntity entity) {
				entity.getController().setState(this);		
			}

			@Override
			public void exitState(LevelEntity entity) {

			}

			@Override
			public void update(LevelEntity entity, float delta) {

			}
			
		};
	}
	
	
	private LevelEntity entity;

	public EnemyController(LevelEntity entity)
	{
		this.entity = entity;
		
		setState(EnemyState.SPAWN);
	}

	//TODO move these to be overriden on demand
	@Override
	public void handleInputPressed(int keycode) {
		//not controlled by input
	}
	@Override
	public void handleInputReleased(int keycode) {
		//not controlled by input
	}

	@Override
	public void update(float delta, Level level) 
	{	
		addTimer(delta);
		getState().update(entity, delta);
		
		if (getState() == EnemyState.SPAWN)
			EnemyState.LEFT.enterState(entity);
		
		TiledMapTileLayer collisionLayer = (TiledMapTileLayer) level.getMap().getLayers().get("Collision");
		
		if (getState() == EnemyState.LEFT)
		{
			if (collisionLayer.getCell((int) (entity.getPosition().x), (int) (entity.getPosition().y)) == null)
				EnemyState.RIGHT.enterState(entity);
			else if (entity.isCollidingLeft())
				EnemyState.RIGHT.enterState(entity);
		}
		
		if (getState() == EnemyState.RIGHT)
		{
			if (collisionLayer.getCell((int) (entity.getBounds().x + 1), (int) (entity.getBounds().y)) == null)
				EnemyState.LEFT.enterState(entity);
			else if (entity.isCollidingRight())
				EnemyState.LEFT.enterState(entity);
		}
	}

	@Override
	public void gotHit(Species hittingEntity) {
		
		if ((getState() != EnemyState.DYING)&&(getState() != EnemyState.DEAD))
			if (hittingEntity.getBounds().getY() > entity.getBounds().getY()+(entity.getBounds().getHeight()*0.8f))
				EnemyState.DYING.enterState(entity);
		
	}

}
