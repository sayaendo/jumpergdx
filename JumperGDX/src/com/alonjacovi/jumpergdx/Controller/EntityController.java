package com.alonjacovi.jumpergdx.Controller;

import com.alonjacovi.jumpergdx.Model.Level;
import com.alonjacovi.jumpergdx.Model.Species;

public interface EntityController
{
	public ActiveState getState();
	public void setState(ActiveState state);
	public ActiveState getPreviousState();
	
	public float getTimer();
	public void resetTimer();
	public void addTimer(float delta);
	
	public void handleInputPressed(int keycode);
	public void handleInputReleased(int keycode);
	
	public void update(float delta, Level level);
	
	public void gotHit(Species player);

}
