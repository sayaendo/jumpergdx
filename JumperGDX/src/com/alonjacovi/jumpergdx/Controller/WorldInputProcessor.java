package com.alonjacovi.jumpergdx.Controller;

import com.alonjacovi.jumpergdx.Model.Level;
import com.badlogic.gdx.InputProcessor;

public class WorldInputProcessor implements InputProcessor
{
	private Level level;
	
	public WorldInputProcessor(Level level) {
		this.level = level;
	}

	@Override
	public boolean keyDown(int keycode) {
		level.getPlayer().handleKeyPressed(keycode, level);
		
		return true;
	}

	@Override
	public boolean keyUp(int keycode) {
		level.getPlayer().handleKeyReleased(keycode, level);
		
		return true;
	}
	
	//not supporting android

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {

		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {

		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {

		return false;
	}

	@Override
	public boolean scrolled(int amount) {

		return false;
	}

}
