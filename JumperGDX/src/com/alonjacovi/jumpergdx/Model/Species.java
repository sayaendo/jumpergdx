package com.alonjacovi.jumpergdx.Model;

import com.alonjacovi.jumpergdx.Controller.EntityController;
import com.alonjacovi.jumpergdx.View.EntityAnimator;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;

public interface Species
{
	
	public String getName();
	public EntityController getController();
	public EntityAnimator getAnimator();
	public Rectangle getBounds();
	
	public LevelEntity getEntity();
	public Array<EntityController> getPassives();
	
	//for passive states, add the array here

}
