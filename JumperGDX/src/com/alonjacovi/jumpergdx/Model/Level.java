package com.alonjacovi.jumpergdx.Model;

import java.util.Iterator;

import com.alonjacovi.jumpergdx.JumperGdxGame;
import com.alonjacovi.jumpergdx.Controller.EnemyController;
import com.alonjacovi.jumpergdx.Controller.PlayerController;
import com.alonjacovi.jumpergdx.Controller.WorldInputProcessor;
import com.alonjacovi.jumpergdx.View.MenuScreen;
import com.alonjacovi.jumpergdx.View.WorldRenderer;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class Level
{
	JumperGdxGame game;
	
	private TiledMap map;
	private WorldRenderer renderer;
	private Species player;
	private int tileSize;
	private WorldInputProcessor input;
	private Array<Enemy> enemies;
//	private Music music;
	
	public Level (JumperGdxGame game) {
		this.game = game;
		
		reload();
		
//		SoundEffects.HIT_ENEMY.play();
		
//		FileHandle file = Gdx.files.internal("world/placeholder.ogg");
//		System.out.print(file.exists());
//		music = Gdx.audio.newMusic(file);
		
//		music = Gdx.audio.newMusic(Gdx.files.internal("world/placeholder.ogg"));
//		music.setLooping(true);
//		music.play();
		
		
		renderer = new WorldRenderer(this);
		
		TiledMapTileLayer layer = (TiledMapTileLayer) map.getLayers().get(0);
		tileSize = (int) layer.getTileHeight();
		
		MapLayer objectLayer = map.getLayers().get("Entities");
		MapObjects objects = objectLayer.getObjects();
		Iterator<MapObject> iter = objects.iterator();
		Vector2 startingPos = new Vector2();
		enemies = new Array<Enemy>(2);
		
		while (iter.hasNext())
		{
			startingPos = new Vector2();
			MapObject object = iter.next();
			if (object instanceof RectangleMapObject)
			{
				if (object.getProperties().get("type", String.class).equals("player"))
				{
					startingPos = ((RectangleMapObject) object).getRectangle().getPosition(startingPos);
					startingPos.x = startingPos.x / tileSize;
					startingPos.y = startingPos.y / tileSize;
					player = new Player(startingPos);
				}
				if (object.getProperties().get("type", String.class).equals("enemy"))
				{
					startingPos =  ((RectangleMapObject) object).getRectangle().getPosition(startingPos);
					startingPos.x = startingPos.x / tileSize;
					startingPos.y = startingPos.y / tileSize;
					enemies.add(new Enemy(startingPos));
				}
			}
		}
		
		input = new WorldInputProcessor(this);
		
		Gdx.input.setInputProcessor(input);
	}
	
	public void update(float delta) {
//		music.play();
		
		player.getEntity().update(delta, this);
		
		if (player.getController().getState() == PlayerController.PlayerState.DEAD)
			game.setScreen(new MenuScreen(game));
		
		for (Enemy enemy : enemies)
		{
			enemy.getEntity().update(delta, this);
			
			if (enemy.getBounds().overlaps(player.getBounds()))
			{
				enemy.getController().gotHit(player);
				player.getController().gotHit(enemy);
			}
		}
		
		Array<Enemy> tempEnemies = new Array<Enemy>(enemies);
		
		
		
		for (Enemy enemy : tempEnemies)
		{
			//every entity should get isDead
			if (enemy.getController().getState() == EnemyController.EnemyState.DEAD)
			{
				enemies.removeValue(enemy, true);
			}
		}
		
		//TODO change to give every entity its own gotHit method that calls on the controller and passives
		
		renderer.render();
	}
	
	public void dispose() {
		map.dispose();
		renderer.dispose();
	}
	
	public LevelEntity getPlayer() {
		return player.getEntity();
	}
	
	public void reload() {
		map = new TmxMapLoader().load("data/level.tmx");
	}

	public TiledMap getMap() {
		return map;
	}

	public int getTileSize() {
		return tileSize;
	}

	public Array<Enemy> getEnemies() {
		return enemies;
	}
	

}
