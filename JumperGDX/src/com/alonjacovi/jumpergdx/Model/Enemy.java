package com.alonjacovi.jumpergdx.Model;

import com.alonjacovi.jumpergdx.Controller.EnemyController;
import com.alonjacovi.jumpergdx.Controller.EntityController;
import com.alonjacovi.jumpergdx.View.EnemyAnimator;
import com.alonjacovi.jumpergdx.View.EntityAnimator;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class Enemy implements Species
{
	//to be generated from JSON instead of having a separate class
		//when JSON'ing: empty constructor, has setEntity method that sets the entity for the species and for all inner components that need the entity reference (FSMs, bounds))
		
		private LevelEntity entity;
		
		public Enemy (Vector2 pos) 
		{
			entity = new LevelEntity(pos, this);
			controller = new EnemyController(entity);
			passive = new Array<EntityController>(1);
			//passive.add(new PlayerRunFsm(entity));
		}

		@Override
		public String getName() {
			return "mari";
		}
		
		private EntityController controller;

		@Override
		public EntityController getController() {
			return controller;
		}
		
		private EntityAnimator animator = new EnemyAnimator();

		@Override
		public EntityAnimator getAnimator() {
			return animator;
		}
		
//		private Rectangle bounds;
		
		@Override
		public Rectangle getBounds() {
//			if (bounds != null)
//				return bounds;
//			else {
				Rectangle bounds = new Rectangle(entity.getPosition().x, entity.getPosition().y, 1, 1);
				return bounds;
//			}
		}

		@Override
		public LevelEntity getEntity() {
			return entity;
		}

		private Array<EntityController> passive;
		
		@Override
		public Array<EntityController> getPassives()
		{
			return passive;
		}
		//also sends out notification for the area the player is currently in


}
