package com.alonjacovi.jumpergdx.Model;

import com.alonjacovi.jumpergdx.Collision;
import com.alonjacovi.jumpergdx.Controller.EntityController;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class LevelEntity
{
	private Vector2 position;
	private Vector2 velocity;
	private Vector2 acceleration;
	
	private Species species;
	
	public LevelEntity(Vector2 pos, Species species) {
		this.position = pos;
		this.velocity = new Vector2();
		this.acceleration = new Vector2();
		
		this.species = species;
	}

	public Vector2 getPosition() {
		return position;
	}
	
	public Vector2 getVelocity() {
		return velocity;
	}
	
	public Vector2 getAcceleration() {
		return acceleration;
	}
	
	public void setPosition(Vector2 pos) {
		this.position = pos;
	}
	

	public void setVelocity(Vector2 vel) {
		this.velocity = vel;
	}
	

	public void setAcceleration(Vector2 acc) {
		this.acceleration = acc;
	}
	

	public void updatePosition(float delta) 
	{
		this.velocity.add(acceleration.cpy().scl(delta));
		this.position.add(velocity.cpy().scl(delta));
		
		if (velocity.x != 0)
			direction.x = Math.signum(velocity.x);
		if (velocity.y != 0)
			direction.y = Math.signum(velocity.y);
		
		if (velocity.y < -10f)
			velocity.y = -10f;
		if (velocity.y > 10f)
			velocity.y = 10f;
	}
	
	public Vector2 tryPosition(float delta)
	{
		Vector2 newVelocity = velocity.cpy().add(acceleration.cpy().scl(delta));
		Vector2 newPosition = position.cpy().add(newVelocity.scl(delta));
		return newPosition;
	}
	
	private Vector2 direction = new Vector2();

	public Vector2 getDirection() {
		return direction;
	}

	public Rectangle getBounds()
	{
		return species.getBounds();
	}

	public Rectangle tryBounds(float delta)
	{
		Rectangle tryBounds = new Rectangle();
		tryBounds.set(getBounds());
		tryBounds.setPosition(tryPosition(delta));
		return tryBounds;
	}
	
	//state machine interfacing

	public void update(float delta, Level level)
	{	
		updatePosition(delta);

		collidingLeft = Collision.isCollidingLeft(level, this);
		collidingRight = Collision.isCollidingRight(level, this);
		collidingUp = Collision.isCollidingUp(level, this);
		collidingDown = Collision.isCollidingDown(level, this);
		
		getController().update(delta, level);
//		getPassives().get(0).update(delta, level);    ***check if there are passives, if so, iterate and update all
		
		velocity.y -= 20f * delta;
		
		float approx = 1f / (float) level.getTileSize();

		if (isCollidingLeft()) {
			if (velocity.x <= 0)
			{
				position.x = -approx + (int) (position.x + 1);
				velocity.x = 0;
			}
		}

		if (isCollidingRight()) {
			if (velocity.x >= 0)
			{
				position.x = approx + (int) position.x;
				velocity.x = 0;
			}
		}
		
		if (isCollidingUp()) {
			position.y = (int) position.y;
			velocity.y = 0;
		}
		
		if (isCollidingDown()) {
			position.y = -approx + (int) (position.y + 1);
			velocity.y = 0;
		}
	}

	public void handleKeyPressed(int keycode, Level level) {
		getController().handleInputPressed(keycode);
		getPassives().get(0).handleInputPressed(keycode);
	}

	public void handleKeyReleased(int keycode, Level level) {
		getController().handleInputReleased(keycode);
		getPassives().get(0).handleInputReleased(keycode);
	}
	
	private boolean collidingLeft;
	private boolean collidingRight;
	private boolean collidingUp;
	private boolean collidingDown;

	public boolean isCollidingLeft() {
		return collidingLeft;
	}

	public boolean isCollidingRight() {
		return collidingRight;
	}

	public boolean isCollidingUp() {
		return collidingUp;
	}

	public boolean isCollidingDown() {
		return collidingDown;
	}

	public void render(TextureAtlas atlas, int tileSize, SpriteBatch batch) {
		species.getAnimator().render(atlas, tileSize, batch, this);
	}
	
	public EntityController getController() {
		return species.getController();
	}
	
	public Array<EntityController> getPassives() {
		return species.getPassives();
	}
	
}
