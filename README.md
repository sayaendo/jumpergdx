#JumperGDX#

##Overview##

JumperGDX is a simple game (mario clone) I've written using LibGDX in Java.
I've used LibGDX for its audio engine, TMX loading, OpenGL graphics, input events, and 2D vector math

The project has my own implementations of Finite State Machines, a physics module, collision detection, and entity structures. The physics module isn't coupled with the collision detection, which means it's easily replaceable (for example, in levels that require slippery or swimming physics). 

##Current Features##

[Video showing the current state, engine is done](https://www.youtube.com/watch?v=4g2ZYvrh1IY) (enemies also die when jumped on)

Currently the game immediately loads into gameplay (as opposed to a menu screen). It reads a single TMX file for its level, and runs that level until the end. The TMX file contains the sprites, level data, enemy data and player data of that level.
The gameplay involves walking and jumping as the player. The enemies roam the level.

The jump is dynamic in the sense that its acceleration and velocity vary depending on the player's velocity and the amount of time the jump button was pressed.